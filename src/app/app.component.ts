import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'scanner';
  scannerType = 'QR';
  isSingleScan = true;
  isScannerOpen = false;
  results = []

  openScanner(isSingleScan: boolean, scannerType = 'QR') {
    this.scannerType = scannerType;
    this.isSingleScan = isSingleScan;
    this.isScannerOpen = true;
  }
  onScan(event: string[]) {
    this.isScannerOpen = false;
    this.results = event;
  }
}
