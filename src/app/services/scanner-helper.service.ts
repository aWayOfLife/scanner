import { Injectable } from '@angular/core';
import * as ZXing from "html5-qrcode/third_party/zxing-js.umd.js"

@Injectable({
  providedIn: 'root'
})
export class ScannerHelperService {

  constructor() { }

  performMonkeyPatches(scannerType = 'QR') {
    console.log("Monkey patching")

    const oldFunc = (ZXing.HTMLCanvasElementLuminanceSource as any).toGrayscaleBuffer;

    let inverterToggle = false;
    (ZXing.HTMLCanvasElementLuminanceSource as any).toGrayscaleBuffer = function (imagebuffer, width, height) {
      const grayscaleBuffer = oldFunc(imagebuffer, width, height);
      const len = grayscaleBuffer.length;
      inverterToggle = !inverterToggle;
      if (inverterToggle) {
        for (let i = 0; i < len; i++) {
          grayscaleBuffer[i] = 0xFF - grayscaleBuffer[i];
        }
      }
      return grayscaleBuffer;
    };
  }
}
