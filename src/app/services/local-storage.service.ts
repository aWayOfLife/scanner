import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  getValue(key: string) {
    const value = localStorage.getItem(key);
    return value;
  }

  setValue(key:string, value: string) {
    const convertedValue = value
    localStorage.setItem(key, convertedValue);
  }
}
