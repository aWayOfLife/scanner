import { TestBed } from '@angular/core/testing';

import { ScannerHelperService } from './scanner-helper.service';

describe('ScannerHelperService', () => {
  let service: ScannerHelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScannerHelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
