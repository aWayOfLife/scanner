import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Html5Qrcode, Html5QrcodeCameraScanConfig } from "html5-qrcode";
import { ScannerHelperService } from '../services/scanner-helper.service';

@Component({
  selector: 'app-scanner-plugin',
  templateUrl: './scanner-plugin.component.html',
  styleUrls: ['./scanner-plugin.component.scss']
})
export class ScannerPluginComponent implements OnInit, OnDestroy {
  
  @Output() onScanComplete = new EventEmitter<string[]>();
  @Input() isSingleScan = true;
  @Input() scannerType = 'QR'

  scanResult: string[] = [];
  currentScanResult: string;
  qrConfig = { fps: 10, qrbox: { width: 300, height: 300 } } as Html5QrcodeCameraScanConfig;
  brConfig = { fps: 30, qrbox: { width: 300, height: 150 } } as Html5QrcodeCameraScanConfig;
  html5QrCode: Html5Qrcode;

  constructor(private scannerHelperService: ScannerHelperService) {
  }

  ngOnInit(): void {
    this.scannerHelperService.performMonkeyPatches(this.scannerType);

    this.html5QrCode = new Html5Qrcode("scanner");

    this.html5QrCode.start(
      { facingMode: "environment" },
      this.scannerType === 'QR' ? this.qrConfig: this.brConfig,
      (decodedText, decodedResult) => {
        console.log('success', decodedText);
        if(this.currentScanResult !== decodedText) {
          this.currentScanResult = decodedText;
          this.scanResult.push(decodedText);
          if(this.isSingleScan) {
            this.complete();
          }
        }
      },
      (error) => { },
    );
    
  }

  close() {
    this.html5QrCode.stop().then((res) => { this.html5QrCode.clear(); this.onScanComplete.emit([]); })
  }

  toggleCamera() {
    console.log('toggle camera');
  }

  complete() {
    this.html5QrCode.stop().then((res) => { this.html5QrCode.clear(); this.onScanComplete.emit(this.scanResult); })
  }

  ngOnDestroy(): void {
    
  }

}
