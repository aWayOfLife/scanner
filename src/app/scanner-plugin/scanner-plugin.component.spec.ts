import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannerPluginComponent } from './scanner-plugin.component';

describe('ScannerPluginComponent', () => {
  let component: ScannerPluginComponent;
  let fixture: ComponentFixture<ScannerPluginComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ScannerPluginComponent]
    });
    fixture = TestBed.createComponent(ScannerPluginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
