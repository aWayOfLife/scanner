import { OnChanges, AfterContentInit, ViewChild, OnDestroy } from '@angular/core';
import { Component, ContentChild, Input, Output, EventEmitter } from '@angular/core';
import { NgxScannerQrcodeComponent, ScannerQRCodeResult } from 'ngx-scanner-qrcode';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnChanges, OnDestroy {

  @Input() isOpen = false;
  @Input() isSingleScan = true;
  @Output() onScanComplete = new EventEmitter<string[]>();
  @ViewChild(NgxScannerQrcodeComponent) action: NgxScannerQrcodeComponent;

  scanResult: ScannerQRCodeResult[] = [];
  currentScanResult: ScannerQRCodeResult;
  devices = [];

  constructor(private localStorageService: LocalStorageService) {}


  ngOnChanges() {
    if(this.isOpen) {
      setTimeout(() => {
        this.startDefaultDevice()
        this.action.devices.subscribe(data => {
          if(data.length) {
            this.devices = data;
          }
        });

      }, 500);
    } else {
      this.action.stop()
    }
  }

  onScanHandler(event:ScannerQRCodeResult[]) {
    this.currentScanResult = event[0];
    this.action.pause();
  }

  retake() {
    this.invalidateCurrentScanResult();
    this.action.stop();
    this.startDefaultDevice();
  }

  save() {
    this.scanResult.push(this.currentScanResult);
    this.invalidateCurrentScanResult();
    this.action.stop();
    if(this.isSingleScan) {
      this.complete();
    } else {
      this.startDefaultDevice();
    }
  }

  close() {
    this.action.stop();
    this.scanResult = [];
    this.onScanComplete.emit([]);
  }

  complete() {
    this.action.stop();
    this.onScanComplete.emit(this.scanResult.map(result => result.value));
  }

  invalidateCurrentScanResult() {
    this.currentScanResult = null;
  }

  toggleCamera() {
    if(this.devices.length > 1) {
        const nextDeviceIndex = this.action.deviceIndexActive === this.devices.length - 1 ? 0 : this.action.deviceIndexActive + 1;
        this.action.playDevice(this.devices[nextDeviceIndex].deviceId);
        this.localStorageService.setValue('defaultDevice', this.devices[this.action.deviceIndexActive].deviceId)
    }
  }

  startDefaultDevice() {
    const defaultDevice = this.localStorageService.getValue('defaultDevice');
    this.action.start(() => {
      if (defaultDevice) {
        this.action.playDevice(defaultDevice)
      }
    });    
  }

  ngOnDestroy(): void {
    this.localStorageService.setValue('defaultDevice', this.devices[this.action.deviceIndexActive].deviceId)
  }




}
